import { useState, useEffect } from 'react';
import { getAllCharacters } from '../query/getCharacters';

const CharacterNotesGenerator = () => {
  const [characters, setCharacters] = useState([]);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const charactersData = await getAllCharacters();
        setCharacters(charactersData);
      } catch (error) {
        console.error('Erreur dans la lecture des personnages:', error);
      }
    };

    const storedUsers = localStorage.getItem("usersList");
    if (storedUsers) {
      setUsers(JSON.parse(storedUsers));
    }

    fetchData();
  }, []);

  useEffect(() => {
    if (characters.length > 0 && users.length > 0) {
      const characterNotes = JSON.parse(localStorage.getItem("characterNotes")) || {};

      // Parcourir chaque personnage
      characters.forEach(character => {
        const notes = characterNotes[character.id] || [];

        // Vérifier si l'utilisateur existe déjà dans la liste des notes pour ce personnage
        users.forEach(user => {
          const existingNote = notes.find(note => note.userId === user.id);
          if (!existingNote) {
            // Si l'utilisateur n'existe pas, ajoutez-le avec une note null
            notes.push({ userId: user.id, note: null });
          }
        });

        // Mettre à jour les notes pour ce personnage
        characterNotes[character.id] = notes;
      });

      // Enregistrer les notes mises à jour dans le localStorage
      localStorage.setItem("characterNotes", JSON.stringify(characterNotes));
    }
    // eslint-disable-next-line
  }, [characters, localStorage.getItem("usersList")]);

  return null;
};

export default CharacterNotesGenerator;
