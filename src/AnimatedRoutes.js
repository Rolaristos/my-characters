import React from 'react'
import { Route, Routes, useLocation } from 'react-router-dom';
import { AnimatePresence } from 'framer-motion';
import Home from './components/home/Home';
import Characters from './components/listing/Characters';
import Perso from './components/detail/Perso';
import Login from './components/login/Login';
import MesFavoris from './components/myfav/MesFavois';

function AnimatedRoutes() {
    const location = useLocation();

    return (
        <AnimatePresence mode='wait'>
            <Routes location={location} key={ location.pathname }>

                {/* Route pour la page d'accueil */}
                <Route path="/" element={<Home />} />

                {/* Route pour la page des personnages */}
                <Route path="/characters" element={<Characters />} />
                <Route path="/myfav" element={< MesFavoris/>} />

                {/* Route pour la page d'un perso */}
                <Route path="/character/:id" element={<Perso />} />

                {/* Route pour la page d'un Login */}
                <Route path="/connexion" element={<Login />} />
            </Routes>
        </AnimatePresence>
    )
}

export default AnimatedRoutes;