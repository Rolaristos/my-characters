import React from 'react';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import './style/homePage.css';
import CharacterNotesGenerator from '../../data/CharactersRating';

const Home = () => {
    return (
        <motion.div
            className="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column homePage"
            initial={{ opacity: 0, scale: 1 }}
            animate={{ opacity: 1, scale: 1 }}
            exit={{ scale: 1.5, opacity: 0, transition: { duration: 1 } }}
            transition={{ duration: 1 }}
        >
            <motion.div
                className="cover-container d-flex w-50 h-100 p-3 flex-column"
                initial={{ x: '-100vw' }}
                animate={{ x: 0 }}
                transition={{ type: 'spring', stiffness: 120 }}
            >
                <CharacterNotesGenerator />
                <svg xmlns="http://www.w3.org/2000/svg" className="d-none">
                    {/* Ajoutez vos symboles SVG ici */}
                </svg>

                <div className="cover-container d-flex w-100 vh-50 p-3 mx-auto mt-auto mb-auto flex-column">
                    <header className="mb-auto">
                        <div>
                            <motion.h1
                                className="float-md-start mb-0 mt-0 mb-0 text-center"
                                style={{ textShadow: '2px 2px 4px rgba(0, 0, 0, 0.5)', fontSize: "2.5em" }}
                                initial={{ opacity: 0, y: -50 }}
                                animate={{ opacity: 1, y: 0 }}
                                transition={{ delay: 0.5, duration: 1, type: 'spring', stiffness: 120 }}
                            >
                                Minami no hanashi - Personnages
                            </motion.h1>
                        </div>
                    </header>

                    <main className="px-3 text-center">
                        <motion.img
                            className="m-2 homePage-img-logo"
                            src="img/icon/logo.png"
                            alt="Waiime Logo"
                            style={{ height: '10em' }}
                            initial={{ opacity: 0, scale: 0 }}
                            animate={{ opacity: 1, scale: 1 }}
                            transition={{ delay: 1, duration: 1, type: 'spring', stiffness: 120 }}
                        />
                        <p className="lead" style={{ fontSize: "2.5em" }}>
                            <Link to="/Characters" className="btn btn-lg btn-light fw-bold border-white homePage-buttonHover">Continuer</Link>
                        </p>
                    </main>

                    <footer className="mt-auto text-white-50 text-center">
                        <p className="text-black-50" style={{ fontSize: "1.5em" }}>
                            Copyright © 2024 Minami no hanashi
                        </p>
                    </footer>
                </div>
            </motion.div>
        </motion.div>
    )
}

export default Home;
