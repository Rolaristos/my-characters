import React, { useState, useEffect } from 'react';

const Login = () => {
    const [loginData, setLoginData] = useState({
        username: localStorage.getItem('username') || '',
        password: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setLoginData({ ...loginData, [name]: value });
    };

    // Fonction pour ajouter un nouvel utilisateur
    const addNewUser = (userData) => {
        // Récupérer la liste des utilisateurs du localStorage
        const usersListJSON = localStorage.getItem('usersList');
        let usersList = [];

        if (usersListJSON) {
            // Convertir la liste JSON en tableau d'objets
            usersList = JSON.parse(usersListJSON);
        }

        // Vérifier le dernier ID utilisé
        let lastId = 0;
        if (usersList.length > 0) {
            lastId = usersList[usersList.length - 1].id;
        }

        // Auto-incrémenter l'ID pour le nouvel utilisateur
        const newId = lastId + 1;
        const newUser = {
            id: newId,
            ...userData,
            favoris: []
        };

        // Ajouter le nouvel utilisateur à la liste
        usersList.push(newUser);

        // Enregistrer la liste mise à jour dans le localStorage
        localStorage.setItem('usersList', JSON.stringify(usersList));
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        // On récupère la liste des utilisateurs depuis le localStorage
        const usersListJSON = localStorage.getItem('usersList');
        let usersList = [];
        if (usersListJSON) {
            // Convertir la chaîne JSON en tableau d'objets
            usersList = JSON.parse(usersListJSON);
        }

        // Vérifier si le pseudo existe déjà dans la liste des utilisateurs
        const existingUser = usersList.find(user => user.username === loginData.username);

        if (existingUser) {
            // Vérifier si le mot de passe correspond
            if (existingUser.password === loginData.password) {
                localStorage.setItem('username', loginData.username);
                window.history.back();
                console.log('Connexion réussie pour l\'utilisateur:', existingUser.username);
            } else {
                console.log('Le pseudo est déjà pris mais le mot de passe est incorrect.');
            }
        } else {
            addNewUser(loginData);
            localStorage.setItem('username', loginData.username);
            window.history.back();
            console.log(localStorage.getItem('usersList'));
        }

        // Réinitialisation des champs après la soumission
        setLoginData({ username: '', password: '' });
    };

    useEffect(() => {
        // Chargement des données depuis le localStorage lors du montage du composant
        const savedUsername = localStorage.getItem('username');
        if (savedUsername) {
            setLoginData((prevData) => ({ ...prevData, username: savedUsername }));
        }
    }, []);

    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <div className="card">
                        <div className="card-body">
                            <h2 className="text-center mb-4">Connexion</h2>
                            <form onSubmit={handleSubmit}>
                                <div className="mb-3">
                                    <label htmlFor="username" className="form-label">Nom d'utilisateur:</label>
                                    <input
                                        type="text"
                                        id="username"
                                        name="username"
                                        value={loginData.username}
                                        onChange={handleChange}
                                        placeholder="Entrez votre nom d'utilisateur"
                                        className="form-control"
                                        required
                                    />
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="password" className="form-label">Mot de passe:</label>
                                    <input
                                        type="password"
                                        id="password"
                                        name="password"
                                        value={loginData.password}
                                        onChange={handleChange}
                                        placeholder="Entrez votre mot de passe"
                                        className="form-control"
                                        required
                                    />
                                </div>
                                <button type="submit" className="btn btn-primary w-100">Se connecter</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
