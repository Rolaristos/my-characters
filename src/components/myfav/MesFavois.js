import React, { useEffect, useState } from 'react';
import { getCharacter } from '../../query/getCharacter';
import { getLikesForUser } from '../../query/getFavorisByUser';
import CharacterCard from '../listing/CharacterCard';
import { Container, Row, Alert } from 'react-bootstrap';
import { motion } from 'framer-motion';

const transition = { duration: 0.5, ease: [0.43, 0.13, 0.23, 0.96] };

const MesFavoris = () => {
    const [characters, setCharacters] = useState([]);
    const indexCharacters = getLikesForUser(localStorage.getItem("username"));

    useEffect(() => {
        const fetchData = async () => {
            try {
                const charactersDataArray = await Promise.all(indexCharacters.map(async index => {
                    const charactersData = await getCharacter(index);
                    return charactersData;
                }));
                setCharacters(charactersDataArray);
            } catch (error) {
                console.error('Error fetching characters:', error);
            }
        };
        fetchData();
        // eslint-disable-next-line
    }, []);

    const goBack = () => {
        window.history.back();
    };

    return (
        <motion.div
            initial={{ opacity: 0, y: 20 }}
            animate={{ opacity: 1, y: 0 }}
            exit={{ opacity: 0, y: -20 }}
            transition={transition}
        >
            <h1 className="mt-5 mb-0 text-center text-light">Mes personnages favoris</h1>
            <div className="d-flex justify-content-center align-items-center">
                <Container>
                    {characters.length === 0 ? (
                        <motion.div
                            initial={{ opacity: 0 }}
                            animate={{ opacity: 1 }}
                            exit={{ opacity: 0 }}
                            transition={transition}
                        >
                            <Alert variant="secondary" className="text-center">Tu n'as encore aucun personnage en favoris !</Alert>
                        </motion.div>
                    ) : (
                        <Row xs={1} md={2} lg={3} className="g-4">
                            {characters.map((character) => (
                                <motion.div
                                    key={character.id}
                                    initial={{ opacity: 0, scale: 0.9 }}
                                    animate={{ opacity: 1, scale: 1 }}
                                    exit={{ opacity: 0, scale: 0.9 }}
                                    transition={transition}
                                >
                                    <CharacterCard character={character} />
                                </motion.div>
                            ))}
                        </Row>
                    )}
                </Container>
            </div>
            {/* Croix en position fixe */}
            <motion.div
                className="close-button"
                onClick={goBack}
                whileHover={{ scale: 1.2, rotate: 45 }}
                whileTap={{ scale: 0.8 }}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                transition={{ duration: 0.5 }}
                title='Retour à la page précédente'
            />
        </motion.div>
    );
}

export default MesFavoris;
