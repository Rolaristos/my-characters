import { useState, useEffect, useRef } from "react";
import { getAllEquipements } from "../../query/getEquipements";
import { motion, useMotionValue, useTransform, AnimatePresence } from "framer-motion";
import { Modal, Button } from "react-bootstrap";
import Chart from 'chart.js/auto';

function Card(props) {
    const [exitX, setExitX] = useState(0);
    const [showModal, setShowModal] = useState(false);
    const [selectedEquipement, setSelectedEquipement] = useState(null);
    const chartRef = useRef(null); // Référence pour le graphique

    const x = useMotionValue(0);
    const scale = useTransform(x, [-150, 0, 150], [0.5, 1, 0.5]);
    const rotate = useTransform(x, [-150, 0, 150], [-45, 0, 45], { clamp: false });

    const variantsFrontCard = {
        animate: { scale: 1, y: 0, opacity: 1 },
        exit: (custom) => ({
            x: custom,
            opacity: 0,
            scale: 0.5,
            transition: { duration: 0.2 }
        })
    };
    const variantsBackCard = {
        initial: { scale: 0, y: 105, opacity: 0 },
        animate: { scale: 0.75, y: 30, opacity: 0.5 }
    };

    function handleDragEnd(_, info) {
        if (info.offset.x < -100) {
            setExitX(-250);
            props.setIndex(props.index + 1);
        }
        if (info.offset.x > 100) {
            setExitX(250);
            props.setIndex(props.index + 1);
        }
    }

    function handleClick(equipement) {
        setSelectedEquipement(equipement);
        setShowModal(!showModal);
    }

    function handleCloseModal() {
        setSelectedEquipement(null);
        setShowModal(false);
    }

    const data = props.data;

    useEffect(() => {
        if (showModal && selectedEquipement) {
            const ctx = document.getElementById('statsChart').getContext('2d');
            
            // Effacer le contenu du canvas
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            
            // Dessiner le nouveau graphique
            chartRef.current = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: Object.keys(selectedEquipement.stats),
                    datasets: [{
                        label: 'Stats',
                        data: Object.values(selectedEquipement.stats)
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        }
    }, [showModal, selectedEquipement]);

    useEffect(() => {
        // Nettoyer le graphique lorsque le modal se ferme
        return () => {
            if (chartRef.current) {
                chartRef.current.destroy();
            }
        };
    }, []);

    return (
        <motion.div
            style={{
                width: 150,
                height: 150,
                position: "absolute",
                top: 0,
                x,
                rotate,
                cursor: "pointer"
            }}
            whileTap={{ cursor: "grabbing" }}
            onClick={() => handleClick(data[props.index % data.length])}
            drag={props.drag}
            dragConstraints={{ top: 0, right: 0, bottom: 0, left: 0 }}
            onDragEnd={handleDragEnd}
            variants={props.frontCard ? variantsFrontCard : variantsBackCard}
            initial="initial"
            animate="animate"
            exit="exit"
            custom={exitX}
            transition={
                props.frontCard
                    ? { type: "spring", stiffness: 300, damping: 20 }
                    : { scale: { duration: 0.2 }, opacity: { duration: 0.4 } }
            }
        >
            <motion.div
                style={{
                    width: 150,
                    height: 150,
                    backgroundImage: data && data.length > 0
                        ? `url('/img/${data[props.index % data.length].image}')`
                        : "none",
                    backgroundSize: "cover",
                    borderRadius: 30,
                    scale
                }}
            />
            <Modal animation={true} show={showModal} onHide={handleCloseModal}>
                <Modal.Header closeButton>
                    <Modal.Title>{selectedEquipement && selectedEquipement.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {selectedEquipement && (
                        <>
                            <p>Type: {selectedEquipement.type}</p>
                            <p className="text-center">Description: {selectedEquipement.description}</p>
                            <p>Stats:</p>
                            <canvas id="statsChart" width="400" height="200"></canvas>
                        </>
                    )}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseModal}>
                        Fermer
                    </Button>
                </Modal.Footer>
            </Modal>
        </motion.div>
    );
}

export function Example({ equippedIds }) {
    const [index, setIndex] = useState(0);
    const [filteredEquipments, setFilteredEquipments] = useState([]);
    const [equipementsData, setEquipementsData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const equipements = await getAllEquipements();
                setEquipementsData(equipements);
            } catch (error) {
                console.error("Error fetching equipements:", error);
            }
        };

        fetchData();
    }, []);

    useEffect(() => {
        const filtered = equipementsData.filter((equipement) =>
            equippedIds.includes(parseInt(equipement.id))
        );
        setFilteredEquipments(filtered);
    }, [equippedIds, equipementsData]);

    return (
        <motion.div style={{ width: 150, height: 150, position: "relative" }}>
            <AnimatePresence initial={false}>
                <Card key={index + 1} frontCard={false} />
                <Card
                    key={index}
                    frontCard={true}
                    index={index}
                    setIndex={setIndex}
                    drag="x"
                    data={filteredEquipments}
                />
            </AnimatePresence>
        </motion.div>
    );
}
