import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { motion } from 'framer-motion';
import { getAllCharacters } from '../../query/getCharacters';
import './style/Characters.css';
import CharacterCard from './CharacterCard';
import CharacterNotesGenerator from '../../data/CharactersRating';
import LazyLoading from './LazyImage'; // Assurez-vous d'importer la bonne classe LazyLoading

const Characters = () => {
  const [characters, setCharacters] = useState([]);
  const [recherche, setRecherche] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [username, setUsername] = useState('');
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const charactersData = await getAllCharacters();
        setCharacters(charactersData);
      } catch (error) {
        console.error('Error fetching characters:', error);
      }
    };

    const savedUsername = localStorage.getItem('username');
    if (savedUsername) {
      setUsername(savedUsername);
      setIsLoggedIn(true);
    }

    fetchData();
    // eslint-disable-next-line
  }, []);



  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
    setRecherche(searchCharacters(e.target.value));
  };

  useEffect(() => {
    // Votre logique d'effet pour vérifier si l'utilisateur est déjà connecté
    const savedUsername = localStorage.getItem('username');
    if (savedUsername) {
      setUsername(savedUsername);
      setIsLoggedIn(true);
    }
  }, []);

  function searchCharacters(searchString) {
    let result = [];
    for (let character of characters) {
      if (character.name.toLowerCase().includes(searchString.toLowerCase())) {
        result.push(character);
      }
    }
    return result;
  }
  
  const handleLogin = () => {
    if (username === '') {
      navigate('/connexion');
    }
  };

  const handleLogout = () => {
    setIsLoggedIn(false);
    if (username !== '') {
      localStorage.removeItem('username');
      setUsername('');
      window.location.reload();
    }
  };

  return (
    <motion.div
      initial={{ opacity: 0, x: -100 }}
      animate={{ opacity: 1, x: 0 }}
      exit={{ x: 2000 }}
      transition={{
        duration: 1.5,
        ease: [0.87, 0, 0.13, 1]
      }}
    >
      <motion.header id='iamthetop'>
        <CharacterNotesGenerator />
        <div className="collapse bg-dark" id="navbarHeader">
          <div className="container">
            <div className="row">
              <div className="col-sm-8 col-md-7 py-4">
                <h4 className="text-white text-center">À propos</h4>
                <p className="text-muted text-justify">
                  L'histoire met en scène Katayama Minami. Jeune lycéenne de 16 ans. Au départ, elle mène sa vie de lycéenne auprès de ses amies et camarades au comportement assez cliché.
                  Au fur et à mesure que les jours passent, Minami devient de plus en plus distrait en cours, et a de plus en plus de mal à se concentrer. En effet, quelque chose occupe ses pensées. Ce sont des sortes de rêves flash, et flou. Un sentiment qui ne semble pas lui appartenir, et qui ne fait que l’oppresser. Cela va tellement la préoccuper, qu'un soir, elle fera un rêve. Rêve lucide, qui reste cependant extrêmement flou et mystérieux. Ce rêve qu'elle baptisera : "Le Mystère" fera plusieurs apparitions durant certaines et formera une sorte de puzzle. Puzzle que devra reconstituer Minami pour trouver la signification du rêve, car oui, le rêve a bel et bien une signification, et les choses que verra Minami durant “Le Mystère”, comme des lieux par exemple, existent bel et bien dans la réalité. Le rêve finira par le conduire vers ░░░▒▒▓▓
                </p>
              </div>
              <div className="col-sm-4 offset-md-1 py-4 text-center">
                {isLoggedIn ? (
                  <>
                    <p className="text-muted">Connecté en tant que {username}</p>
                    <Link className="btn btn-info" to="/myfav">Voir mes likes</Link>
                    <button onClick={handleLogout} className="btn btn-danger">Déconnexion</button>
                  </>
                ) : (
                  <button onClick={handleLogin} className="btn btn-primary">Connexion</button>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="navbar navbar-dark bg-dark shadow-sm">
          <div className="container">
            <Link to="/" className="navbar-brand d-flex align-items-center">
              <img src="/img/icon/logo.png" width="20" height="20" alt='logo'></img>
              <strong>Minami no hanashi</strong>
            </Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
          </div>
        </div>
      </motion.header>

      <main className='characters-main-observer'>

        <div className='characters-main2'>
          <section className="py-5 text-center container">
            <div className="row py-lg-5">
              <div className="col-lg-6 col-md-8 mx-auto">
                <h1 className="fw-light">Galerie des personnages</h1>
                <p className="lead text-muted">Découvrez l'apparence, ainsi que les traits de personnalités de vos personnages favoris !</p>
                <p>
                  <Link to="/Characters" className="btn btn-primary my-2">Personnages</Link>
                  <Link to="/Equipements" className="btn btn-secondary my-2">Équipements</Link>
                </p>
              </div>
            </div>
          </section>
        </div>

        <div className="album py-5 bg-light" >
          <div className="container">
            <div className='d-flex'>
              <h1 className="d-flex mr-auto">Recherche</h1>
              <input
                type="text"
                placeholder="Rechercher un personnage..."
                value={searchTerm}
                onChange={(e) => handleSearch(e)} />
            </div>
            {recherche.length === 0 ? (
              <>
                <h1 className='text-muted text-center'>Recherchez votre personnage</h1>
              </>
            ) : (
              <>
                <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                  {recherche.map((character) => (
                      <CharacterCard character={character} blur={true} />
                  ))}
                </div>                
              </>
            )}
          </div>

          <div className="container">
            <h1 className='d-flex'>Famille Katayama</h1>
            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              {characters
                .filter(character => character.name.toLowerCase().includes('katayama'))
                .map((character) => (
                    <CharacterCard character={character} blur={true} />
                ))}
            </div>
          </div>

          <div className="container">
            <h1 className='d-flex'>Personnages secondaires</h1>
            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              {characters
                .filter(character => character.role.toLowerCase().includes('personnage secondaire'))
                .map((character) => (
                    <CharacterCard character={character} blur={true} />
                ))}
            </div>
          </div>

          <div className="container">
            <h1 className='d-flex'>Classe 2-2</h1>
            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              {characters
                .filter(character => character.profession.toLowerCase().includes('lycéen'))
                .map((character) => (
                    <CharacterCard character={character} blur={true} />
                ))}
            </div>
          </div>

          <div className="container">
            <h1 className='d-flex'>Personnages tertiaires</h1>
            <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              {characters
                .filter(character => character.role.toLowerCase().includes('personnage tertiaire'))
                .map((character) => (
                    <CharacterCard character={character} blur={true} />
                ))}
            </div>
          </div>
        </div>
      </main>

      <footer className="text-muted py-5 text-center">
        <div className="container">
          <p className="float-end mb-1">
            <a href="#iamthetop">Retour en haut</a>
          </p>
          <p className="mb-1">Découvrez l'univers ainsi que les personnages de Minami no Hanashi sur différentes plateformes !</p>
          <p className="mb-0">Sur : <a href="https://www.webnovel.com/fr/book/minami-no-hanashi-yume_28893074908572605">Webnovel</a>, <a href="https://www.royalroad.com/fiction/82165/minami-no-hanashi-yume">Royal Road</a> ou encore sur <a href="https://www.wattpad.com/user/MinamiNoHanashi">Wattpad</a></p>
        </div>
      </footer>

      <script src="/docs/5.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossOrigin="anonymous"></script>

    </motion.div>
  );
};

export default Characters;
