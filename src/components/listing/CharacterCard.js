import React from 'react';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import { useInView } from 'react-intersection-observer';
import LikeButton from '../card/LikeButton';
import { LazyLoadImage } from 'react-lazy-load-image-component';


const CharacterCard = ({ character, blur }) => {
  const { ref, inView } = useInView();

  return (
    <div className="characters-card" ref={ref}>
      {inView && (
        <>
          <motion.div
            initial={{ opacity: 0, y: 50 }}
            animate={{ opacity: 1, y: 0 }}
            whileHover={{ y: -10 }}
            whileTap={{ scale: 0.95 }}
            transition={{ duration: 0.5, delay: character.id / 10 }}
          >
            <Link to={`/character/${character.id}`} key={character.id}>
              <div className="col" style={{ margin: "1rem" }}>
                <div className="card shadow-sm">
                  <div className="characters-img d-flex justify-content-center align-items-center" style={{ height: "225px" }}>
                    <LazyLoadImage
                      className={`bd-placeholder-img card-img-top ${blur ? "img-blur" : ""}`}
                      style={{ objectFit: "contain" }}
                      alt={"Image de " + character.name}
                      height="225"
                      src={`img/characters/${character.image[0]}`}
                      placeholderSrc="img/640x360.png"
                    />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">{character.name}</h5>
                    <p className="card-text">{character.description}</p>
                    <div className="d-flex justify-content-end align-items-right">
                      <small className="text-muted text-end">{character.role}</small>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
          </motion.div>
          <LikeButton character={character} defaultPos={true} />
        </>
      )}
    </div>
  );
};

export default CharacterCard;
