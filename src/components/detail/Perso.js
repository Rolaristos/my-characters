import React, { useState, useEffect } from 'react';
import { getCharacter } from '../../query/getCharacter';
import './style/Perso.css';
import { useParams } from 'react-router-dom';
import { Carousel, Button, Modal } from 'react-bootstrap';
import StarRating from '../card/StarRating';
import LikeButton from '../card/LikeButton';
import './style/ratingSection.css';
import { motion } from 'framer-motion';
import { Example } from '../listing/Equipements';

export default function Perso() {
    const { id } = useParams();
    const [imgList, setImageList] = useState([]);
    const [character, setCharacter] = useState({});
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const charactersData = await getCharacter(id);
                setCharacter(charactersData);
                setImageList(charactersData.image);
            } catch (error) {
                console.error('Error fetching characters:', error);
            }
        };

        fetchData();
    }, [id]);

    // Fonction pour ouvrir la fenêtre modale
    const handleShowModal = () => {
        setShowModal(true);
    };

    // Fonction pour fermer la fenêtre modale
    const handleCloseModal = () => {
        setShowModal(false);
    };

    // Fonction pour retourner à la page précédente
    const goBack = () => {
        window.history.back();
    };

    return (
        <motion.main
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 0.5 }}
            className='perso-img'
        >
            <Carousel fade>
                {imgList.map((image, index) => (
                    <Carousel.Item key={index}>
                        <img
                            className="d-block w-100"
                            src={require(`../../../public/img/characters/${image}`)}
                            alt={`Slide ${index}`}
                            style={{ width: "100%", height: "100vh", objectFit: 'contain' }}
                        />
                        <Carousel.Caption>
                            <p>{character.attributes?.height}</p>
                            <p>{character.attributes?.weight}</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                ))}
            </Carousel>
            <div className='persos-handleStats'>
                <StarRating onView={false} characterId={character.id} data={localStorage.getItem("characterNotes")} />
                <LikeButton character={character} defaultPos={false} />
                <StarRating onView={true} characterId={character.id} data={localStorage.getItem("characterNotes")} />
            </div>

            <div className='text-center pb-5'>
                <Button variant="secondary" onClick={handleShowModal}>Voir les détails</Button>
            </div>
            {/* Fenêtre modale Bootstrap */}
            <Modal show={showModal} onHide={handleCloseModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Fiche personnage</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Nom: {character.name}</p>
                    <p>Oeuvre: {character.oeuvre}</p>
                    <p>Age: {character.attributes?.age}</p>
                    <p>Profession: {character.profession}</p>
                    <p>Club: {character.club}</p>
                    <p>Points forts:</p>
                    <ul>
                        {character.attributes?.strengths?.map((strength, index) => (
                            <li key={index}>{strength}</li>
                        ))}
                    </ul>
                    <p>Points faibles:</p>
                    <ul>
                        {character.attributes?.weaknesses?.map((weakness, index) => (
                            <li key={index}>{weakness}</li>
                        ))}
                    </ul>
                    <p>Quelques mots:</p>
                    <ul>
                        {character.attributes?.personality?.map((phrase, index) => (
                            <li key={index}>{phrase}</li>
                        ))}
                    </ul>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseModal}>Fermer</Button> {/* Bouton pour fermer la fenêtre modale */}
                </Modal.Footer>
            </Modal>
            {
                character.equipement && character.equipement.length > 0 &&
                <div className="example-container">
                    <h1 className='text-center text-light'>Équipements du personnage</h1>
                    <Example equippedIds={character.equipement} />
                </div>
            }
            {/* Croix en position fixe */}
            <motion.div
                className="close-button"
                onClick={goBack}
                whileHover={{ scale: 1.2, rotate: 45 }}
                whileTap={{ scale: 0.8 }}
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                transition={{ duration: 0.5 }}
                title='Retour à la page précédente'
            />
        </motion.main>
    )
}
