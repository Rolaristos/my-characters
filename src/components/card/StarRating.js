import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import './star.css';

function StarRating({ onView, characterId, data }) {
    const [rating, setRating] = useState(0);
    const [averageRating, setAverageRating] = useState(0);
    const characterData = localStorage.getItem("characterNotes");
    const [currentUserId, setUserId] = useState();
    const [userNote, setUserNote] = useState(null);
    const navigate = useNavigate();

    useEffect(() => {
        if (localStorage.getItem("username") != null) {
            const userList = localStorage.getItem("usersList");
            JSON.parse(userList).forEach(user => {
                // eslint-disable-next-line
                if (user.username == localStorage.getItem("username")) {
                    setUserId(user.id);
                    return;
                }
            });
        }
    }, []);

    useEffect(() => {
        if (onView) {
            // Parsez la chaîne JSON en un objet JavaScript
            const parsedData = JSON.parse(data);

            // Obtenez les votes pour l'identifiant de personnage spécifié
            const votes = parsedData[characterId];
            console.log(votes);

            // Calculez la moyenne des notes
            let total = 0;
            let count = 0;
            if (votes) {
                Object.values(votes).forEach((vote) => {
                    if (vote && vote.note !== null) {
                        total += vote.note;
                        count++;
                    }
                });
            }

            if (count > 0) {
                const average = total / count;
                setAverageRating(average);
            }
        }
    }, [onView, characterId, data]);

    useEffect(() => {
        if (!onView && characterData) {
            // Récupérer la note attribuée par l'utilisateur pour ce personnage à partir du local storage
            const parsedData = JSON.parse(characterData);
            const userData = parsedData[characterId];
            if (userData) {
                const user = userData.find(entry => entry.userId === currentUserId);
                if (user) {
                    setUserNote(user.note);
                }
            }
        }
    }, [characterData, characterId, currentUserId, onView]);

    const handleRatingChange = (value) => {
        if (!onView) {
            setRating(value);
        }
    };

    const handleRating = (value) => {
        if (localStorage.getItem("username") == null) {
            navigate("/connexion");
        }
        if (!onView) {
            // Mettre à jour le rating dans le localStorage
            const characterNotes = JSON.parse(characterData);
            const newData = characterNotes[characterId];
            console.log(newData);
            newData.forEach(user => {
                // eslint-disable-next-line
                if (user.userId == currentUserId) {
                    user.note = value;
                    return;
                }
            })
            characterNotes[characterId] = newData;
            localStorage.setItem("characterNotes", JSON.stringify(characterNotes));
            // Mettre à jour le state pour afficher le nouveau rating
            setRating(value);
            setUserNote(value); // Mettre à jour la note de l'utilisateur
        }
    };

    return (
        <div className="radio-input">
            
            {[5, 4, 3, 2, 1].map((value) => (
                <label key={value} className={`star s${value} ${onView ? 'no-hover' : ''}`} style={{ '--color': (onView && value <= averageRating) ? 'yellow' : (userNote !== null && value <= userNote) ? 'yellow' : 'white' }}>
                    <input
                        type="radio"
                        id={`value-${value}`}
                        name="value-radio"
                        value={value}
                        checked={rating === value}
                        onChange={() => handleRatingChange(value)}
                        disabled={onView}
                        onClick={() => handleRating(value)}
                    />
                </label>
            ))}
            {onView && <p>Average Rating: {averageRating.toFixed(1)}</p>}
        </div>
    );
}

export default StarRating;
