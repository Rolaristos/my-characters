export const getLikesForUser = (username) => {
    // Récupérer la liste des utilisateurs depuis le localStorage
    const usersListJSON = localStorage.getItem('usersList');
    if (usersListJSON) {
        // Convertir la chaîne JSON en tableau d'objets
        const usersList = JSON.parse(usersListJSON);

        // Trouver l'utilisateur avec le nom d'utilisateur spécifié
        const user = usersList.find(user => user.username === username);

        if (user) {
            // Retourner la liste de likes de l'utilisateur trouvé
            return user.favoris;
        } else {
            console.log('Utilisateur non trouvé');
            return [];
        }
    } else {
        console.log('Aucun utilisateur enregistré');
        return [];
    }
};